---
title: '20A ITT1 PROGRAMMING'
subtitle: 'Lecture plan'
filename: '20A_ITT1_PROGRAMMING_lecture_plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2020-06-15
email: 'nisi@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan - tentative

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology programming, oeait20, 20A
* Name of lecturer and date of filling in form: NISI, 2020-06-15
* Title of the course, module or project and ECTS: Programming, 6 ECTS
* Required readings, literature or technical instructions and other background material: 
    * Python For Everybody book [https://www.py4e.com/book.php](https://www.py4e.com/book.php) - [(CC license)](https://creativecommons.org/licenses/by-nd/3.0/legalcode)
    * Jetbrains PyCharm [https://www.jetbrains.com/pycharm/](https://www.jetbrains.com/pycharm/)
    * Socratica Python Programming [https://www.youtube.com/playlist?list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-](https://www.youtube.com/playlist?list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-)


See [https://eal-itt.gitlab.io/20a-itt1-programming](https://eal-itt.gitlab.io/20a-itt1-programming) for detailed daily plan, links, references, exercises and so on.



| INIT | Week  | Content                                            |
|:---- |:----- |:-------------------------------------------------- |
| NISI | 37    | Introduction to Python programming course  
| NISI | 38    | Variables, operators, expressions and statements  
| NISI | 39    | Conditional execution, logical operators, flow charts, exeptions
| NISI | 40    | Functions 
| NISI | 41    | Iteration
| NISI | 43    | Strings
|      |       | PyCharm IDE - license and installation
| NISI | 44    | Files
| NISI | 45    | Lists
| NISI | 46    | Dictionaries
| NISI | 47    | Tuples
| NISI | 48    | Regular expressions
| NISI | 49    | Networked programs
| NISI | 50    | Using webservices
| NISI | 2     | Object-oriented programming
| NISI | 3     | Databases and SQL
|      |       | Recap quiz and evaluation

---------------------------------------------------------


# General info about the course, module or project


## The student’s learning outcome

Learning goals for the course are described in the curriculum section 2 about the national elements.

## Content

The subject area includes fundamentals of programming, the use of environments and data handling, as well as design, development, testing and documentation of solutions.
The course is based on the book Python For Everybody [https://www.py4e.com/book.php](https://www.py4e.com/book.php) and covers the basics of programming in Python. The course is emphasized on data analysis.

## Method

The course is build using cooperative learning structures. The course uses exercises from Python For Everybody [https://www.py4e.com/book.php](https://www.py4e.com/book.php) as well as exercises targeted for IoT applications.

To facilitate different learning styles videos from Socratica and Charles Severance is used as supplementary material.

Exercises will be documented by students with version control using git and gitlab.  

## Equipment

Computer capable of running Python. Windows will be used in examples and screenshots.

## Projects with external collaborators  (proportion of students who participated)
None at this time.

## Test form/assessment
Programming is a part of the 1st year part 1 exam.

See semester description on itslearning for details.

## Other general information
None at this time.
