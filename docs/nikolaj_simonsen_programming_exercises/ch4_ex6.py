"""
Exercise 6: Rewrite your pay computation with time-and-a-half for over-time
and create a function called compute pay which takes two parameters(hours and rate).
"""


def main():
    try:
        hours = float(input('Enter Hours: '))
        rate = float(input('Enter rate: '))
        compute_pay(hours, rate)
    except ValueError:
        print('Error, please enter numeric input')
        main()


def compute_pay(_hours, _rate):
    regular_hours = 40
    over_rate = _rate * 1.5

    if _hours > 40:
        over_hours = _hours - regular_hours
        print('You worked overtime this week!\n')
        print('Pay: ' + str((over_hours * over_rate) + (regular_hours * _rate)))
    else:
        print('Pay: ' + str(_hours * _rate))


if __name__ == '__main__':
    main()
