"""
1.  Create the class in a Python file, name the class Person()
2.  Add the following attributes
    1.  fullname (string)
    2.  birthdate (datetime formatted as YYYYMMDD)
    3.  address (list with streetname, streetnumber, city, postalcode, countrycode)
    4.  gender (male, female, other)
3.  Add the following methods
    1.  getfirstname(return str(firstname)
    2.  getlastname(return str(lastname)
    3.  getage(return int(age)
    4.  getaddress (only returnlist(streetname, streetnumber, city))
    5.  getfullinfo (return all info from the above methods as a dictionary)
4.  create at least 3 persons from your class and test your methods
"""

import datetime

class Person:

    def __init__(self, fullname, birthdate, address, gender):
        self.fullname = fullname
        self.first_name = self.fullname.split(' ')[0]
        self.last_name = self.fullname.split(' ')[1]
        self.birthdate = birthdate
        self.address = address
        self.gender = gender

    def get_first_name(self):
        return self.first_name

    def get_last_name(self):
        return self.last_name

    def get_age(self):
        self.age = 0
        now = datetime.datetime.now()
        year = int(self.birthdate[0:4])
        month = int(self.birthdate[4:6])
        day = int(self.birthdate[6:8])
        b_date = datetime.datetime(year, month, day)
        age_days = (now - b_date).days
        age_years = age_days / 365
        return age_years

    def get_address(self):
        self.short_address = self.address[0:3]
        return  self.short_address

    def get_full_info(self):
        return {'first_name': self.get_first_name(), 'last_name': self.get_last_name(), 'age': self.get_age(), 'address': self.get_address()}

address = ['Testvej', 2, 'Odense', '5000', 'DK']
person1 = Person('Nikolaj Simonsen', '20000402', address, 'male')

print(person1.get_first_name(), person1.get_last_name(), person1.get_age(), *person1.get_address())
print(person1.get_full_info())


