"""
Exercise 1: Write a function called chop that takes a list and modifies
it, removing the first and last elements, and returns None.
Then write a function called middle that takes a list and returns a new list that
contains all but the first and last elements.
"""

fruits_list = ['apple', 'banana', 'orange', 'kiwi', 'cherry']
vegetable_list = ['potato', 'cucumber', 'tomato', 'pumpkin', 'lettuce']


def print_list(_list):
    for item in _list:
        print(item)


def chop(_list):
    _list.pop(0)
    _list.pop(len(_list)-1)
    return None


def middle(_list):
    new_list = _list[1:len(_list)-1]
    return new_list


if __name__ == '__main__':
    print_list(fruits_list)
    print('\n')
    chop(fruits_list)
    print_list(fruits_list)
    print('\n')
    print_list(vegetable_list)
    print('\n')
    new_vegetable_list = middle(vegetable_list)
    print_list(new_vegetable_list)
    print('\n')
    print_list(vegetable_list)


