"""
Exercise 5: What will the following Python program print out?
"""


def fred():
    print("ZAP")


def jane():
    print("ABC")


jane()
fred()
jane()