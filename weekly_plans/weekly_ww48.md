---
Week: 48
Content:  Regular Expressions
Material: See links in weekly plan
Initials: NISI
---

# Week 48 ITT1-programming - Regular Expressions

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 11 exercises
* Test Python programs presented in chapter 12 (not the exercises)

### Learning goals

The student can:

* Use regular expressions

The student knows: 

* Basic syntax of regular expressions
* When to use regular expressions

## Deliverables

* Chapter 11 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Friday 2020-11-27 (A + B class)  
Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 9:00 Introduction to the day 
* 9:15 Student presentations  
    Students shows how they solved chapter 10 exercises from py4e
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 11
    * Watch chapter 11 videos 
* 10:30 Hands-on time
* 11:30 Lunch break
* 12:15 Hands-on time
* 13:45 Evaluation of the day + Q&A  
        To help you reflect on your own learning and give feedback to improve the lectures, answer the reflection questions at [https://forms.gle/PF5YX6SyrMgCXQg56](https://forms.gle/PF5YX6SyrMgCXQg56)
        
* 14:00 Hands-on time
* 16:15 End of day


## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.

## Comments

PY4E Chapter 11 video lessons:

* part 1 [https://youtu.be/ovZsvN67Glc](https://youtu.be/ovZsvN67Glc)
* part 2 [https://youtu.be/fiar4QZZ7Xo](https://youtu.be/fiar4QZZ7Xo)
* part 3 [https://youtu.be/GiQdXo2Bvgc](https://youtu.be/GiQdXo2Bvgc)

RegEx online parser and bulder:

* [regex101.com](https://regex101.com/)

PY4E Chapter 12 video lessons:

* [part 1](https://youtu.be/RsnaRPC52G0)
* [part 2](https://youtu.be/Bvx7vY454xw)
* [part 3](https://youtu.be/Lr9Vm-VghAk)
* [part 4](https://youtu.be/-cmlmaVSONg)
* [part 5](https://youtu.be/k1sUxGPpQOk)
* [part 6](https://youtu.be/D7ZI8--qbBw)

PY4E Chapter 12 worked examples:
* [socket](https://youtu.be/EqUyu8ZZYUE)  
* [urllib](https://youtu.be/jKaCKIdIoks)  
* [beautifulsoup](https://youtu.be/mhaHWiSPxxE)  
