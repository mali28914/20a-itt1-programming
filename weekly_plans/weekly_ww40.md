---
Week: 40
Content:  Functions
Material: See links in weekly plan
Initials: NISI
---

# Week 40 19A-ITT1-programming - Functions

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Read chapter 4 in Python For Everybody
* Chapter 4 exercises, in Python For Everybody, completed

### Learning goals

The student can implement:

* Functions
* Built in functions
* Math functions
* User defined functions
* Function parameters and arguments

## Deliverables

* Chapter 4 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Thursday 2020-10-01 (B class) and Friday 2020-10-02 (A class)

* 9:00 Introduction to the day 
* 9:15 Student presentations  
    Students shows how they solved chapter 3 exercises from py4e
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 4
    * Watch chapter 4 videos 
* 10:30 Hands-on time
* 11:30 Lunch break
* 12:15 Hands-on time
* 13:45 Evaluation of the day + Q&A  
        To help you reflect on your own learning and give feedback to improve the lectures, answer the reflection questions at [https://forms.gle/PF5YX6SyrMgCXQg56](https://forms.gle/PF5YX6SyrMgCXQg56)
        
* 14:00 Hands-on time
* 16:15 End of day


## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.

## Comments

PY4E Chapter 4 video lessons:

* part 1 [https://youtu.be/5Kzw-0-DQAk](https://youtu.be/5Kzw-0-DQAk)
* part 2 [https://youtu.be/AJVNYRqn8kM](https://youtu.be/AJVNYRqn8kM)