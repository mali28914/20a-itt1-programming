---
Week: 38
Content:  Programming basics
Material: See links in weekly plan
Initials: NISI
---

# Week 38 ITT1-programming - Basics

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Read chapter 2 in Python For Everybody
* Python For Everybody - Chapter 2 exercises completed 

### Learning goals

The student can:

* Perform daily GIT operations

The student knows:

* Programming basics
    * commenting code
    * input and output
    * value types
    * operators
    * variables
    * statements
    * reserved words
    * mnemonic naming
    * concatenation

## Deliverables

* Chapter 2 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Thursday 2019-09-17 (B class) and Friday 2019-09-18 (A class)  
Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 9:00 Introduction to the day 
* 9:15 Student presentations  
    Students shows how they solved chapter 1 exercises from py4e
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 2
    * Watch chapter 2 videos 
* 10:30 Hands-on time
* 11:30 Lunch break
* 12:15 Hands-on time
* 13:45 Evaluation of the day + Q&A  
        To help you reflect on your own learning and give feedback to improve the lectures, answer the reflection questions at [https://forms.gle/n2v8kG6H2tQrjrma8](https://forms.gle/n2v8kG6H2tQrjrma8)      
* 14:00 Hands-on time
* 16:15 End of day

## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.

## Comments

PY4E Chapter 2 video lessons:

* part 1 [https://youtu.be/7KHdV6FSpo8](https://youtu.be/7KHdV6FSpo8)
* part 2 [https://youtu.be/kefrGMAglGs](https://youtu.be/kefrGMAglGs)

Socratica videos covering some of the topics from chapter 2 and 3:

* Python strings [https://youtu.be/iAzShkKzpJo](https://youtu.be/iAzShkKzpJo)
* Numbers in Python Version 3 [https://youtu.be/_87ASgggEg0](https://youtu.be/_87ASgggEg0)
* Arithmetic in Python 3 [https://youtu.be/Aj8FQRIHJSc](https://youtu.be/Aj8FQRIHJSc)