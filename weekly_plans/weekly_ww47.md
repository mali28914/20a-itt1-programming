---
Week: 47
Content:  Tuples
Material: See links in weekly plan
Initials: NISI
---

# Week 47 ITT1-programming - Tuples

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 10 exercises
* Work on your own challenge

### Learning goals

The student can:

* Use the tuples datatype
* Install 3rd party modules in PyCharm
* Use matplotlib to visualize data

The student knows: 

* The difference between tuples and othe Python datatypes

## Deliverables

* Chapter 10 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Thursday 2020-11-19 (B class) and Friday 2020-11-20 (A class)

* 9:00 Introduction to the day 
* 9:15 Student presentations  
    Students shows how they solved chapter 9 exercises from py4e
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 10
    * Watch chapter 10 videos 
* 10:30 Hands-on time
* 11:30 Lunch break
* 12:15 Hands-on time
* 13:45 Evaluation of the day + Q&A  
        To help you reflect on your own learning and give feedback to improve the lectures, answer the reflection questions at [https://forms.gle/PF5YX6SyrMgCXQg56](https://forms.gle/PF5YX6SyrMgCXQg56)
        
* 14:00 Hands-on time
* 16:15 End of day


## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.

## Comments

PY4E Chapter 10 video lessons:

* part 1 [https://youtu.be/CaVhM65wD6g](https://youtu.be/CaVhM65wD6g)
* part 2 [https://youtu.be/FdUdA6o0Ij0](https://youtu.be/FdUdA6o0Ij0)