---
Week: 49
Content:  Networked programs
Material: See links in weekly plan
Initials: NISI
---

# Week 49 ITT1-programming - Networked programs

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 12 exercises 

### Learning goals

The student can:

* Use TCP sockets and libraries to get data via HTTP and HTTPS
* Develop programs using pair programming
* Reflect on own learning

The student knows: 

* Basics of TCP and HTTP(S)

## Deliverables

* Chapter 12 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Thursday 2020-12-03 (B class) and Friday 2020-12-04 (A class)

* 9:00 Introduction to the day 
* 9:15 Student presentations  
    Students shows how they solved chapter 11 exercises from py4e
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 12
    * Watch chapter 12 videos 
* 10:30 Hands-on time
* 11:30 Lunch break
* 12:15 Hands-on time
* 13:45 Evaluation of the day + Q&A  
        To help you reflect on your own learning and give feedback to improve the lectures, answer the reflection questions at [https://forms.gle/PF5YX6SyrMgCXQg56](https://forms.gle/PF5YX6SyrMgCXQg56)
        
* 14:00 Hands-on time
* 16:15 End of day


## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.

## Comments

PY4E Chapter 12 video lessons:

* part 1 [https://youtu.be/RsnaRPC52G0](https://youtu.be/RsnaRPC52G0)
* part 2 [https://youtu.be/Bvx7vY454xw](https://youtu.be/Bvx7vY454xw)
* part 3 [https://youtu.be/Lr9Vm-VghAk](https://youtu.be/Lr9Vm-VghAk)
* part 4 [https://youtu.be/-cmlmaVSONg](https://youtu.be/-cmlmaVSONg)
* part 5 [https://youtu.be/k1sUxGPpQOk](https://youtu.be/k1sUxGPpQOk)
* part 6 [https://youtu.be/D7ZI8--qbBw](https://youtu.be/D7ZI8--qbBw)

PY4E Chapter 12 worked examples:
* socket [https://youtu.be/EqUyu8ZZYUE](https://youtu.be/EqUyu8ZZYUE)  
* urllib [https://youtu.be/jKaCKIdIoks](https://youtu.be/jKaCKIdIoks)  
* beautifulsoup [https://youtu.be/mhaHWiSPxxE](https://youtu.be/mhaHWiSPxxE)  