---
Week: 02
Content:  Object-oriented programming
Material: See links in weekly plan
Initials: NISI
---

# Week 02 ITT1-programming - Object-oriented programming

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Object-oriented programming exercises 

### Learning goals

The student can:

* Create simple classes
* Instantiate objects from classes
* Extend classes

The student knows: 

* What a class is
* Why classes are useful
* Class attributes
* Class methods
* Class constructors
* The ```self``` keyword

## Deliverables

* Chapter 14 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Thursday 2021-01-07 (B class) and Friday 2021-01-08 (A class)

* 9:00 Introduction to the day 
* 9:15 Student presentations  
    Students shows how they solved chapter 13 exercises from py4e
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 6
    * Watch chapter 6 videos 
* 10:30 Video: Python Classes and Objects [https://youtu.be/apACNr7DC_s](https://youtu.be/apACNr7DC_s)
* 10:45 Hands-on time
* 11:30 Lunch break
* 12:15 Hands-on time
* 13:45 Evaluation of the day + Q&A  
        To help you reflect on your own learning and give feedback to improve the lectures, answer the reflection questions at [https://forms.gle/PF5YX6SyrMgCXQg56](https://forms.gle/PF5YX6SyrMgCXQg56)
        
* 14:00 Hands-on time
* 16:15 End of day

## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.

## Comments

PY4E Chapter 14 video lessons:

* part 1 [https://youtu.be/u9xZE5t9Y30](https://youtu.be/u9xZE5t9Y30)
* part 2 [https://youtu.be/b2vc5uzUfoE](https://youtu.be/b2vc5uzUfoE)

