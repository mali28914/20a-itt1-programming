---
Week: 50
Content:  Web services
Material: See links in weekly plan
Initials: NISI
---

# Week 50 ITT1-programming - Using web services

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 13 exercises 

### Learning goals

The student can:

* Programatically retreive JSON data
* Programatically communicate with an API
* Parse JSON data

The student knows: 

* What XML is
* What JSON is
* What web services are
* What api's are
* What a service oriented architecture is

## Deliverables

* Chapter 13 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Thursday 2020-12-10 (B class) and Friday 2020-12-11 (A class)

* 9:00 Introduction to the day 
* 9:15 Student presentations  
    Students shows how they solved chapter 12 exercises from py4e
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 13
    * Watch chapter 13 videos 
* 10:30 Hands-on time
* 11:30 Lunch break
* 12:15 Hands-on time
* 13:45 Evaluation of the day + Q&A  
        To help you reflect on your own learning and give feedback to improve the lectures, answer the reflection questions at [https://forms.gle/PF5YX6SyrMgCXQg56](https://forms.gle/PF5YX6SyrMgCXQg56)
        
* 14:00 Hands-on time
* 16:15 End of day

## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.

## Comments

PY4E Chapter 13 video lessons:

* part 1 [https://youtu.be/7NEtEctD9Cw](https://youtu.be/7NEtEctD9Cw)
* part 2 [https://youtu.be/A8KcBx9153Y](https://youtu.be/A8KcBx9153Y)
* part 3 [https://youtu.be/0cA6W-4JPQ4](https://youtu.be/0cA6W-4JPQ4)
* part 4 [https://youtu.be/J5DjteDzgoM](https://youtu.be/J5DjteDzgoM)
* part 5 [https://youtu.be/SNeJcvBY-h4](https://youtu.be/SNeJcvBY-h4)
* part 6 [https://youtu.be/QyHcOL3C7fQ](https://youtu.be/QyHcOL3C7fQ)
* part 7 [https://youtu.be/mrRo2xX39nw](https://youtu.be/mrRo2xX39nw)

PY4E Chapter 13 worked exercises:

* JSON [https://youtu.be/RGQfDirZ7_s](https://youtu.be/RGQfDirZ7_s)
* GeoJSON API [https://youtu.be/vjQZscHOaG4](https://youtu.be/vjQZscHOaG4)
* Twitter API [https://youtu.be/zJzPyEPCbXs](https://youtu.be/zJzPyEPCbXs)


