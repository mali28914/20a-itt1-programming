---
Week: 46
tags:
- Dictionaries
- Programming challenge
---

# Exercises for ww46
 
The cooperative learning structures can be found at [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf)

## Exercise 0 - PY4E chapter 9 knowledge QUIZ

This is a teacher paced quiz with questions about PY4E chapter 9 - Dictionaries

Go to [https://b.socrative.com/login/student/](https://b.socrative.com/login/student/) and write **simonsen7368** as the room number

Answer the first question and wait. When everyone have answered a question you will get the next question.

## Exercise 1 - Your own challenge knowledge sharing (group)

### Information

In a group of 4 people use **Team interview** to share your knowledge about the work you do in your own challenge.

### Exercise instructions

Preparation: Every team member writes 2-3 questions that they would like to ask.  

You have 5 minutes for each interview, I will keep track of time.

## Exercise 2 - Work on your own challenge

Like last week you now have time scheduled in class to work on you own challenge.
I will be in class to answer questions and help.

If you are still researching you are doing it wrong. The purpose of doing your own challenge is to get your hands dirty and write Python code.
The research should happen when you run into problems during coding.
The topic of the project is not that important, as long as you practice and write python code

You have until lunch

\pagebreak