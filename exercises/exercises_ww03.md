---
Week: 03
tags:
- SQL
- Databases
- sqlite
---

# Exercises for ww03

## Exercise 1 - PY4E database introduction

### Information

Together we will watch [part 1 - introduction](https://youtu.be/3RMPveOMd0k)

## Exercise 0 - Install sqlitebrowser 

### Information

To learn the SQL language you need a small program that can query a database called sqlitebrowser.

### Exercise instructions

1. Download and install [sqlitebrowser](https://sqlitebrowser.org/dl/)

You probably need to restart your computer during the installation process

2. Open the application to ensure that it works

## Exercise 1 - Single Table SQL 

### Information

This exercise teaches you the basic SQL CRUD statements using sqlitebrowser

### Exercise instructions

1. Download the [database handout](https://www.py4e.com/lectures3/Pythonlearn-15-Database-Handout.txt) document and open it
2. Watch [part 2 - Single Table SQL](https://youtu.be/yRJE-nk20sM)
3. Follow along in sqlitebrowser using the database handout file

## Exercise 2 - Data modelling and multiple tables

### Information

Together we will watch 

[part 3 - Complex models](https://youtu.be/rHjRpYUl5eg)

and 

[part 4 - Database relationships](https://youtu.be/PgE--P-ZWvU)

## Exercise 3 - Artist database

### Information

This exercise teaches you how to create a multi table database using sqlitebrowser

### Exercise instructions

1. Watch [part 5 - Foreign keys](https://youtu.be/GfuH_8uH16k)
2. Follow along in sqlitebrowser using the database handout file

## Exercise 4 - Join

### Information

This exercise teaches you how to use join across tables which is the way you query data from multiple tables using foreign keys.

### Exercise instructions

1. Watch [part 6 - Join](https://youtu.be/zMOSVrb82iU)
2. Follow along in sqlitebrowser using the database handout file

## Exercise 5 - Many-to-many relationships

### Information

This exercise teaches you how to use many-to-many relationships between tables.

### Exercise instructions

1. Watch [part 7 - many to many](https://youtu.be/uq_Wf4nuXqE)
2. Follow along in sqlitebrowser using the database handout file

## Exercise 6 - Add more entries to the artist database

### Information

Before moving to using sqlite in Python add some more data to the artist database 

### Exercise instructions

1. Add at least 2 more artists, albums, genres and tracks to the database using sqlitebrowser and SQL statements
2. You can use dummy data that you make up or use something like [discogs](https://www.discogs.com/)

## Exercise 7 - Itunes database in Python

### Information

All of the things you have been doing in sqlitebrowser can of course be done from Python.
This exercise teaches you how to use SQL together with Python

### Exercise instructions

1. Download and unzip the files in [tracks.zip](https://www.py4e.com/code3/tracks.zip)
2. Open the folder in PyCharm
3. Watch the [video](https://youtu.be/UGe7X_L-lHc) and follow along 

## Exercise 8 - Artist database in Python

### Information

To practice using SQL together with Python i reccomend that you create the artist database using a ython application

### Exercise instructions

1. Using PY4E chapter 15 create a Python application that creates the artist database but without using sqlitebrowser
2. Query the database using the same SQL statements as in exercise 4, of course from within the Python script

## Exercise 9 - Email database in Python  (optional but recommended)

### Information

To practice using SQL together with Python I recommend that you follow this worked example that puts mbox.txt into a sqlite database

### Exercise instructions

1. Download [emaildb.py](https://www.py4e.com/code3/emaildb.py)
2. Open the file in PyCharm
3. Watch the [video](https://youtu.be/UGe7X_L-lHc) and follow along