---
Week: 45
tags:
- List
- Programming challenge
---

# Exercises for ww45
 
The cooperative learning structures can be found at [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf)

## Exercise 0 - PY4E chapter 8 knowledge (group)

### Information

In a group of 4 people use **think-pair-share** to share your prepared knowledge about: 

* How do you create a list ?
* What element types can a list hold ?
* What it means that lists are mutable ?
* How do you traverse a list ? 
* How can you concatenate lists ?
* What is the output of ```[7,6,5] * 2``` 
* How do you slice a list ?
* What is the return value of the pop list method ?
* How do you sum a list of numbers ?
* If you have multiple variable assigned to the same list, what impact will it have to change one of the variables ?

### Exercise instructions

For all question:  
Think: ? minutes
Pair: ? minutes
Share: ? minutes

## Exercise 1 - PY4E chapter 8 knowledge (class)

Use **30 for tea** to answer:

* What element types can a list hold ?
* What it means that lists are mutable ?
* What is the return value of the pop list method ?
* How do you sum a list of numbers ?
* If you have multiple variable assigned to the same list, what impact will it have to change one of the variables ?

## Exercise 2 - Work on your own challenge

As promised you have time scheduled in class to work on you own challenge.
Use the opportunity to ask your classmates about advice if you have problems or would like advice to solve a particular problem in your challenge.

\pagebreak