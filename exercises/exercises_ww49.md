---
Week: 49
tags:
- TCP
- Sockets
- HTTP(S)
- Protocols
- Socket
- Web scraping
- RFC
---

# Exercises for ww49
 
The cooperative learning structures can be found at [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf)

## Exercise 0 - PY4E chapter 12 knowledge sharing (Group) 

Last week you had time in class to study chapter 12 in PY4E, both videos and reading including.
A week can seem a long time and this exercise recaps and helps you remember the knowledge you aquired.

### Exercise instructions

Use think-pair-share to answer these questions:

1. What are RFC's (Ready for comment) ?
2. What are Protocols ? (especially HTTP and HTTPS)
3. What are sockets ?
4. What is web scraping
5. When do you use a binary file instead of a text file ?
6. How does the things you have already learned, fit into chapter 12 ?
7. From chapter 12, what was the most important thing you learned ?

Thinking: ? minutes  
Pairing: ? minutes  
Sharing: ? minutes  

## Exercise 1 - PY4E chapter 12 Knowledge sharing (Class) 

To ensure that everyone in class has the same knowledge you have to share it between groups in class.

### Exercise instructions

1. Use **3 for tea** to share your groups answer to questions 1 - 6

? minutes in different groups
? minutes back in your own group

## HTTP Video break

Together we will watch:

What happens when you click a link [https://youtu.be/keo0dglCj7I](https://youtu.be/keo0dglCj7I)

## Exercise 2 - PY4E chapter 12 Exercise 1 

### Information

Start by watching this video on pair programming in practice [https://youtu.be/ET3Q6zNK3Io](https://youtu.be/ET3Q6zNK3Io) to recap the basics of pair programming

Use pair programming [https://resources.collab.net/agile-101/pair-programming](https://resources.collab.net/agile-101/pair-programming) to solve the first exercise from chapter 12 in PY4E

### Exercise instructions

1. Team up in pairs
2. Rewrite the exercise instruction in to **Pseudo code** (logical steps in you program)
3. Write the program following the logic steps, change role for each logic step

## Exercise 3 - PY4E chapter 12 Exercise 2 

### Information

Use pair programming [https://resources.collab.net/agile-101/pair-programming](https://resources.collab.net/agile-101/pair-programming) to solve the second exercise from chapter 12 in PY4E

### Exercise instructions

1. Team up in new pairs if needed/possible
2. Rewrite the exercise instruction in to **Pseudo code** (logical steps in you program)
3. Write the program following the logic steps, change role for each logic step

## Exercise 4 - PY4E chapter 12 Exercise 3 

### Information

Use pair programming [https://resources.collab.net/agile-101/pair-programming](https://resources.collab.net/agile-101/pair-programming) to solve the third exercise from chapter 12 in PY4E

### Exercise instructions

1. Team up in new pairs if needed/possible
2. Rewrite the exercise instruction in to **Pseudo code** (logical steps in you program)
3. Write the program following the logic steps, change role for each logic step

## Exercise 5 - PY4E chapter 12 Exercise 4 

### Information

Use pair programming [https://resources.collab.net/agile-101/pair-programming](https://resources.collab.net/agile-101/pair-programming) to solve the fourth exercise from chapter 12 in PY4E

### Exercise instructions

1. Team up in new pairs if needed/possible
2. Rewrite the exercise instruction in to **Pseudo code** (logical steps in you program)
3. Write the program following the logic steps, change role for each logic step 

\pagebreak