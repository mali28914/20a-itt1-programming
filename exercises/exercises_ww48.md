---
Week: 48
tags:
- grep
- Tag B
- Protocols
- Socket
- Web scraping
- RFC
- Regular expressions
---

# Exercises for ww48

## Exercise 0 - PY4E chapter 11 Exercise 1 

### Exercise instructions

1. Solve PY4E chapter 11 exercise 1 
2. Show it to me when you are done

Exercise 1: Write a simple program to simulate the operation of the
grep command on Unix.  
Ask the user to enter a regular expression and
count the number of lines that matched the regular expression:

$ python grep.py  
Enter a regular expression: ^Author  
mbox.txt had 1798 lines that matched ^Author  

$ python grep.py  
Enter a regular expression: ^X-  
mbox.txt had 14368 lines that matched ^X-  

$ python grep.py  
Enter a regular expression: java$  
mbox.txt had 4175 lines that matched java$  

you have 45 minutes to solve the exercise

**remember to document in your gitlab programming project with seperate .py files. Filename syntax: chX_exX.py** 

## Exercise 1 - PY4E chapter 11 Exercise 2 

### Exercise instructions

1. Solve PY4E chapter 11 exercise 2 
2. Show it to me when you are done

Exercise 2: Write a program to look for lines of the form:  
New Revision: 39772

Extract the number from each of the lines using a regular expression
and the findall() method.  
Compute the average of the numbers and print out the average.

Enter file:mbox.txt  
38444.0323119

Enter file:mbox-short.txt  
39756.9259259

you have 45 minutes to solve the exercise  

**remember to document in your gitlab programming project with seperate .py files. Filename syntax: chX_exX.py** 

## Exercise 2 - PY4E chapter 12 Videos 

### Information

Because chapter 12 is quite extensive and introduces a lot of new things it is important that you do everything you can to prepare.

### Exercise instructions

Watch the PY4E Chapter 12 video lessons:

* [part 1](https://youtu.be/RsnaRPC52G0)
* [part 2](https://youtu.be/Bvx7vY454xw)
* [part 3](https://youtu.be/Lr9Vm-VghAk)
* [part 4](https://youtu.be/-cmlmaVSONg)
* [part 5](https://youtu.be/k1sUxGPpQOk)
* [part 6](https://youtu.be/D7ZI8--qbBw)

## Exercise 3 - PY4E chapter 12 Reading 

### Information

In the chapter 12 text there is a lot of examples, while reading try to type them into a Python program and run them (+ save them!). You also need to install a couple of 3rd party modules which you will also need for the chapter 12 exercises next week.

### Reading guide

1. Solve examples while you read
Focus on these keuwordswhile reading:
2. What are RFC's (Ready for comment) ?
3. What are Protocols ? (especially HTTP and HTTPS)
4. What are sockets ?
5. What is web scraping
6. When do you use a binary file instead of a text file ?
7. How does the things you have already learned, fit into chapter 12 ?
7. The "Bonus section for Unix / Linux users" matters to you because you are all using a Raspberry Pi with Linux. The information about Curl and WGET is essential.


\pagebreak