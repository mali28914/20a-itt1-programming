---
Week: 41
tags:
- Iteration
- Loops
- Programming challenge
- Functions
- Debugging
---

# Exercises for ww41
 
The cooperative learning structures can be found at [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf)

## Exercise 0 - PY4E chapter 5 knowledge (group)

### Information

This exercise recaps and shares knowledge, on a group level, about PY4E chapter 5.

### Exercise instructions

In a group of 4 people use **Think-pair-share** to discuss your understanding of:

1. What while loops are and how you can use them ?
2. What for loops are and how you can use them ?
3. What is `break` and `continue` used for in loops? 
4. Can you nest while and for loops and what would you use it for? 
5. What is an iterator and how do you use it to count loop iterations? 

For all question:  
Think: 10 minutes
Pair: 5 minutes
Share: 5 minutes

Note your answers for use in exercise 1

\pagebreak

## Exercise 1 - PY4E chapter 5 knowledge (class)

### Information

This exercise recaps and shares knowledge, on class level, about PY4E chapter 5.

### Exercise instructions

Use **three for tea** to share the knowledge gathered in exercise 0.

Use 10 minutes in different groups and then 5 minutes in your original groups.

I will keep track of time and tell you when to switch.

\pagebreak

## Exercise 2 - Programming challenge

### Information

Individually solve the `Loops` challenge at hackerrank

### Exercise instructions

1. Solve the loops challenge [https://www.hackerrank.com/challenges/python-loops/problem](https://www.hackerrank.com/challenges/python-loops/problem)   
**All test cases must be passed when submitting your code**

You have 15 minutes to solve the `Loops` challenge.  

## Exercise 3 - Pair programming challenge

### Information

In pairs of two students use **Pair programming** to program a simple calculator  

You have until 13:00 to complete the code.  
If you finish before, make a flow chart that documents the program, or expand it with more calculator functions like squaring etc.

Remember to push your code to your gitlab programming repository when you are done.

### Exercise instructions

Requirements for the calculator program are:

1. Define 4 functions to add, subtract, divide or multiply 2 float numbers
2. If the user inputs `done` at any time the program will stop and print the message `goodbye` and exit
3. If the user enters anything else than numbers or `done` the program should catch a ValueError and print the message `only numbers and "done" is accepted as input, please try again` and then restart
4. If the user tries to divide by zero the program should catch a ZeroDivisionError and print the message `cannot divide by zero, please try again` and then restart 
5. On successful calculation the program should restart and ask the user to enter new numbers again

The program must pass the following test cases:

**test 1**  
input:
```
enter the first number
> 2
enter the second number
> 2
would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?
> 1
```

output: 
```
2.0 added with 2.0 = 4.0
``` 

**test 2**  
input:
```
enter the first number
> 2
enter the second number
> 2
would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?
> 2
```

output: 
```
2.0 subtracted from 2.0 = 0.0
``` 

**test 3**  
input:
```
enter the first number
> 2
enter the second number
> 2
would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?
> 3
```

output: 
```
2.0 divided with 2.0 = 1.0
``` 

**test 4**  
input:
```
enter the first number
> 2
enter the second number
> 2
would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?
> 4
```

output: 
```
2.0 multiplied with 2.0 = 4.0
```

**test 5**  
input:
```
enter the first number
> 2
enter the second number
> 0
would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?
> 3
```

output: 
```
cannot divide by zero, please try again
```

**test 6**  
input:
```
enter the first number
> this_is_cool
enter the second number
> another_invalid_input
would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?
> thirty
```

output: 
```
only numbers and "done" is accepted as input, please try again
```

**test 7**  
input:
```
enter the first number
> done
```

output: 
```
goodbye
```

**test 8**  
input:
```
enter the first number
> 2
enter the second number
> done
```

output: 
```
goodbye
```

**test 9**  
input:
```
enter the first number
> 2
enter the second number
> 2
would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?
> done
```

output: 
```
goodbye
```