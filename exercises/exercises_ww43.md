---
Week: 43
tags:
- Debugging
- Programming challenge
- String
---

# Exercises for ww43
 
The cooperative learning structures can be found at [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf)

## Exercise 0 - PY4E chapter 6 knowledge (group)

### Information

In a group of 4 people use **think-pair-share** to recap your knowledge about: 

* How do you access individual characters in a string ? 
* How can you traverse a string ?
* What is a string slice ?
* What it means that strings are immutable ?
* How do you compare two strings ?
* What string methods do you know ?
* How do you extract a substring from a string

### Exercise instructions

For all questions:  
Think: 10 minutes
Pair: 5 minutes
Share: 5 minutes

## Exercise 1 - PY4E chapter 6 knowledge (class)

### Information

This exercise recaps and shares knowledge, on class level, about PY4E chapter 6.

### Exercise instructions

Use **three for tea** to share the knowledge gathered in exercise 0.

Use 10 minutes in different groups and then 10 minutes in your original groups.

## Exercise 2 - Programming challenge

Individually solve the `String Split and Join` challenge at hackerrank

### Exercise instructions

1. Solve the String Split and Join challenge [](https://www.hackerrank.com/challenges/python-string-split-and-join/problem)  
**All test cases must be passed when submitting your code**

You have 20 minutes to solve the `String Split and Join` challenge.  

\pagebreak

## Exercise 3 - Pair programming challenge

In pairs of 2 students, write a number guessing game for 2 players, human vs. computer.

### Exercise instructions

Requirements for the number guessing game are:

1. On game start calculate a random number between 0 and 9
2. The computer will give you 3 attempts to guess the number, otherwise the computer wins the round
3. If the user enters anything else than a number print the message `Thats not even a number.... restarting` and then restart the program
4. If the user wins print `Damn, you won!, try again? (y/n)`   
5. If the computer wins, print `na na I won :-P - try again? (y/n)` 
6. If user enters `y` at the end of a round, restart the program
7. If the user enters `n` at the end of a round, print the total rounds score between human and computer

Test the program with following test cases:

**test 1 - Invalid user input**  
input:
```
enter your guess between 0 and 9 > ulahhhh
```

output: 
```
Thats not even a number.... restarting
enter your guess between 0 and 9 >
``` 

**test 2 - 1st or 2nd wrong guess**  
input:
```
enter your guess between 0 and 9 > 3
```

output: 
```
wrong guess, try again!
enter your guess between 0 and 9 >
``` 

**test 3 - 3rd wrong guess**  
input:
```
enter your guess between 0 and 9 > 3
```

output: 
```
na na I won :-P - try again? (y/n) >
```

**test 4 - correct guess**  
input:
```
enter your guess between 0 and 9 > 4
```

output: 
```
Damn, you won!, try again? (y/n) >
```

**test 5 - end game**  
input:
```
Damn, you won!, try again? (y/n) > n
```

output: 
```
computer wins: 1 human wins: 1 see ya...
```
**bonus if you finish early:**  

1. Implement functionality that will stop the program when either human or computer reach 5 won rounds
2. If the user enters an arbitrary string which has a number, extract the number from the string and use it as a guess. ie. `sdoijwer6poi`  

You are free to search relevant information on the internet to solve the bonus tasks.  
If you solve both of the bonus tasks and show it in class, you will be rewarded with a cup of coffee

\pagebreak