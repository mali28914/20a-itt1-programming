---
Week: 40
tags:
- Functions
- Pair programming
- Programming challenge
---

# Exercises for ww40
 
The cooperative learning structures can be found at [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf)

## Exercise 0 - PY4E chapter 4 knowledge (group)

### Information

This exercise recaps and shares knowledge, on a group level, about PY4E chapter 4.

### Exercise instructions

In a group of 4 people use **Think-pair-share** to discuss your understanding of:

1. What built-in Python functions do you know?
2. How are user functions defined in Python?
3. What are function parameters and arguments?
4. How do you return values from functions?
4. How do you call a function and assign the return value to a variable? 

For all questions:  
Think: 10 minutes
Pair: 5 minutes
Share: 5 minutes

Note your answers for use in exercise 1

\pagebreak

## Exercise 1 - PY4E chapter 4 knowledge (class)

### Information

This exercise recaps and shares knowledge, on class level, about PY4E chapter 4.

### Exercise instructions

Use **three for tea** to share the knowledge gathered in exercise 0.

Use 15 minutes in different groups and then 10 minutes in your original groups.

I will keep track of time and tell you when to switch.

\pagebreak

## Exercise 2 - Pair programming challenge

### Information

In pairs of two students - use **Pair programming** to solve the `Write a function` challenge at hackerrank

### Exercise instructions

1. Create an account on hackerrank [https://www.hackerrank.com/domains/python](https://www.hackerrank.com/domains/python)
2. Read about pair-programming [http://www.extremeprogramming.org/rules/pair.html](http://www.extremeprogramming.org/rules/pair.html)
3. Solve the hello world challenge to get familiar with hackerrank [https://www.hackerrank.com/challenges/py-hello-world/problem](https://www.hackerrank.com/challenges/py-hello-world/problem) 
4. Use pair programming to solve the Write a function challenge [https://www.hackerrank.com/challenges/write-a-function/problem]()  
**All test cases must be passed when submitting your code**

I will keep track of time and tell you when to switch roles.

You have until lunch to solve the `Write a function` challenge.  
If you finish before time proceed to the Python if-else challenge [https://www.hackerrank.com/challenges/py-if-else/problem](https://www.hackerrank.com/challenges/py-if-else/problem)   

**TIP** If you need an overview you can make a flowchart