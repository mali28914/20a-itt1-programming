---
Week: 47
tags:
- Tuple
- Matplotlib
- Visualization
---

# Exercises for ww47
 
The cooperative learning structures can be found at [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf)

## Exercise 0 - PY4E chapter 10 knowledge (group) 

### Information

This exercise recaps knowledge about PY4E chapter 10 which is about the datatype tuples. If you read chapter 10 you are ready for the exercise.

### Exercise instructions

In a group of 4 people use **round table** to discuss your understanding of tuples.

1. How can you create a tuple?
2. Can you change tuples after instantiation?
3. How does the slice operator work on tuples?
4. How do you compares tuples?
5. How does sort work on a tuple?   
6. How do you convert a dictionary to a list of tuples?
7. How do you sort a dictionary into a list ordered by value using tuples

You have ? minutes.

## Exercise 1 - PY4E chapter 10 knowledge (class)

### Information

This exercise shares the group knowledge between groups.

### Exercise instructions

Use **three for tea** to share the knowledge gathered in exercise 0.

Use ? minutes in different groups and then ? minutes in your original groups.

## Exercise 2 - Matplotlib visualization

### Information

This exercise is about using a 3rd party module called matplotlib [https://matplotlib.org/](https://matplotlib.org/). It is a library for visualizing data in various diagrams and charts (line, bar, pie etc.)

Since we have been making histograms in the exercises for chapter 9 we will use those as the data input for the charts.

We will use the matplotlib video series [https://www.youtube.com/playlist?list=PL-osiE80TeTvipOqomVEeZ1HRrcEvtZB_](https://www.youtube.com/playlist?list=PL-osiE80TeTvipOqomVEeZ1HRrcEvtZB_) by Corey Schafer

### Exercise instructions

1. install matplotlib in your pycharm project [https://www.jetbrains.com/help/pycharm/installing-uninstalling-and-upgrading-packages.html](https://www.jetbrains.com/help/pycharm/installing-uninstalling-and-upgrading-packages.html)
2. Make a line chart of the data from PY4E ch9_ex2 or ch9_ex5 following video1 [https://youtu.be/UO98lJQ3QGI](https://youtu.be/UO98lJQ3QGI) just skip the install part if you are on pycharm (start at 2:00)  
Important! matplotlib uses seperate lists for the x and y axis.  
ch9_ex2 and ch9_ex5 will give you a dictionary that you have to convert into one list containing the keys and another list containing the values.  
3. Make a bar chart of the data from PY4E ch9_ex2 or ch9_ex5 following video1 [https://youtu.be/UO98lJQ3QGI](https://youtu.be/UO98lJQ3QGI)  

**Bonus challenge** Make a chart of choice from ch10_Ex3 

\pagebreak